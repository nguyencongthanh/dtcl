<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('champions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar');
            $table->char('origin');
            $table->char('class');
            $table->char('cost');
            $table->char('rank');
            $table->char('updown');
            $table->string('health');
            $table->string('sm');
            $table->string('mana');
            $table->string('armor');
            $table->string('mr');
            $table->string('dps');
            $table->string('damage');
            $table->string('as');
            $table->string('cr');
            $table->string('range');
            $table->string('skill_name');
            $table->string('skill_img');
            $table->text('skill_info');
            $table->text('update');
            $table->char('item1');
            $table->char('item2');
            $table->char('item3');
        });
        Schema::create('baseitems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('stats');
            $table->string('img');
            $table->text('update');
        });
        Schema::create('combineditems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('stats');
            $table->string('img');
            $table->char('rank');
            $table->char('updown');
            $table->text('abilities');
            $table->text('update');
            $table->char('item1');
            $table->char('item2');
        });
        Schema::create('origins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('rank');
            $table->string('abilities_img');
            $table->text('abilities_info');
        });
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('rank');
            $table->string('abilities_img');
            $table->text('abilities_info');
        });
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('rank');
            $table->string('updown');
            $table->char('c1');
            $table->char('c2');
            $table->char('c3');
            $table->char('c4');
            $table->char('c5');
            $table->char('c6');
            $table->char('c7');
            $table->char('c8');
            $table->char('mvp1');
            $table->char('mvp2');
            $table->char('mvp3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
