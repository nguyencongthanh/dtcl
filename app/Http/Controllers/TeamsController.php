<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index()
    {
        //Champions
        $champions = DB::table('champions')->get();
        $items = DB::table('combineditems')->get();
        //Teams
        $teams = DB::table('teams')->get();
        $teams_s = $teams->where('rank', '=', 's');
        $teams_a = $teams->where('rank', '=', 'a');
        return view(
            'teams',
            [
                //Champions Items
                'champions' => $champions,
                'items' => $items,
                //Teams
                'teams' => $teams,
                'teams_s' => $teams_s,
                'teams_a' => $teams_a
            ]
        );
    }
}