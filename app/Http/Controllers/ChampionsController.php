<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChampionsController extends Controller
{
    public function index()
    {
        //Champions
        $champions = DB::table('champions')->get();
        $champions_s = $champions->where('rank', '=', 's');
        $champions_a = $champions->where('rank', '=', 'a');
        $champions_b = $champions->where('rank', '=', 'b');
        $champions_c = $champions->where('rank', '=', 'c');
        $champions_d = $champions->where('rank', '=', 'd');
        $champions_e = $champions->where('rank', '=', 'e');
        //Origins
        $origins = DB::table('origins')->get();
        //Classes
        $classes = DB::table('classes')->get();
        //Combined Items
        $combineditems = DB::table('combineditems')->get();
        return view(
            'champions',
            [
                //Champions
                'champions' => $champions,
                'champions_s' => $champions_s,
                'champions_a' => $champions_a,
                'champions_b' => $champions_b,
                'champions_c' => $champions_c,
                'champions_d' => $champions_d,
                'champions_e' => $champions_e,
                //Origins
                'origins' => $origins,
                //Classes
                'classes' => $classes,
                //Combined Items
                'combineditems' => $combineditems
            ]
        );
    }
}
