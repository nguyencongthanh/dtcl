<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function index()
    {
        //Champions
        $baseitems = DB::table('baseitems')->get();
        $combineditems = DB::table('combineditems')->get();
        $combineditems_s = $combineditems->where('rank', '=', 's');
        $combineditems_a = $combineditems->where('rank', '=', 'a');
        $combineditems_b = $combineditems->where('rank', '=', 'b');
        $combineditems_c = $combineditems->where('rank', '=', 'c');
        $combineditems_d = $combineditems->where('rank', '=', 'd');
        $combineditems_e = $combineditems->where('rank', '=', 'e');
        return view(
            'items',
            [
                //Items
                'baseitems' => $baseitems,
                'combineditems' => $combineditems,
                'combineditems_s' => $combineditems_s,
                'combineditems_a' => $combineditems_a,
                'combineditems_b' => $combineditems_b,
                'combineditems_c' => $combineditems_c,
                'combineditems_d' => $combineditems_d,
                'combineditems_e' => $combineditems_e
            ]
        );
    }
}