@extends('layouts.back')

@section('content')
<div class="container">
    <h4>Teams</h4>
    <a class="waves-effect waves-light btn"><i class="material-icons left">add</i>button</a>
    <table class="highlight centered">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>avatar</th>
                <th>origin</th>
                <th>class</th>
                <th>cost</th>
                <th>rank</th>
                <th>updown</th>
                <th>health</th>
                <th>sm</th>
                <th>mana</th>
                <th>armor</th>
                <th>mr</th>
                <th>dps</th>
                <th>damage</th>
                <th>as</th>
                <th>cr</th>
                <th>range</th>
                <th>skill_name</th>
                <th>skill_img</th>
                <!-- <th>skill_info</th> -->
                <th>update</th>
                <th>item1</th>
                <th>item2</th>
                <th>item3</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($champions as $champion)
            <tr>
                <td>{{$champion->id}}</td>
                <td>{{$champion->name}}</td>
                <td>{{$champion->avatar}}</td>
                <td>{{$champion->origin}}</td>
                <td>{{$champion->class}}</td>
                <td>{{$champion->cost}}</td>
                <td>{{$champion->rank}}</td>
                <td>{{$champion->updown}}</td>
                <td>{{$champion->health}}</td>
                <td>{{$champion->sm}}</td>
                <td>{{$champion->mana}}</td>
                <td>{{$champion->armor}}</td>
                <td>{{$champion->mr}}</td>
                <td>{{$champion->dps}}</td>
                <td>{{$champion->damage}}</td>
                <td>{{$champion->as}}</td>
                <td>{{$champion->cr}}</td>
                <td>{{$champion->range}}</td>
                <td>{{$champion->skill_name}}</td>
                <td>{{$champion->skill_img}}</td>
                <!-- <td>{{$champion->skill_info}}</td> -->
                <td>{{$champion->update}}</td>
                <td>{{$champion->item1}}</td>
                <td>{{$champion->item2}}</td>
                <td>{{$champion->item3}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection