@extends('layouts.front')

@section('content')
<script src="{{ asset('js/items.js') }}" defer></script>
<title>Trang bị - Đấu trường chân lý</title>
<div class="row">

    <div class="col s12 l3">
        <!-- <div class="input-field search-champions top-col-search">
            <input id="last_name" type="text" class="validate">
            <label for="last_name">Tìm kiếm trang bị</label>
        </div> -->
        <div class="col s12 line-bottom top-col">
            <div class="col s12 left-align">
                <h2 class="title">Danh sách trang bị</h2>
            </div>
        </div>
        <div class="col s12 no-padding">
            <ul class="collapsible popout items">
                <li class="active">
                    <div class="collapsible-header"><i class="material-icons">call_made</i><span>Trang bị cơ bản</span></div>
                    <div class="collapsible-body">
                        @foreach($baseitems as $baseitem)
                        <a class="btn-floating waves-effect waves-light red item base modal-trigger" data-id="{{$baseitem->id}}" href="#modal_list_combined">
                            <img src="/img/items/base/{{$baseitem->img}}">
                        </a>
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">call_split</i><span>Trang bị kết hợp</span></div>
                    <div class="collapsible-body">
                        @foreach($combineditems as $combineditem)
                        <a class="btn-floating waves-effect waves-light red item modal-trigger" href="#modal_combined_item" data-id="{{$combineditem->id}}">
                            <img src="/img/items/combined/{{$combineditem->img}}">
                        </a>
                        @endforeach                        
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="col s12 l9 line-left">
        <div class="col s12 line-bottom top-col">
            <div class="col s12 m6 left-align">
                <h2 class="title">Bảng xếp hạng các trang bị</h2>
            </div>
            <div class="col m6 right-align hide-on-med-and-down">
                <div class="input-field search-champions">
                    <input id="search" type="text" class="validate autocomplete">
                    <label for="search">Tìm kiếm</label>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="col s6 left-align">
                <h3 class="title">Phiên bản hiện tại 9.18</h3>
            </div>
            <div class="col s6 right-align">
                <span class="text-up-rank">Tăng sức mạnh</span> <span class="text-down-rank">Giảm sức mạnh</span>
            </div>
        </div>
        <table id="table_items">
            <tbody>
                <tr class="row-rank">
                    <td class="column-rank s">s</td>
                    <td>
                        @foreach ($combineditems_s as $combineditem_s)
                        <div class="items-chip">
                            <a class="chip {{$combineditem_s->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_s->id}}">
                                <img src="/img/items/combined/{{$combineditem_s->img}}">
                                <span class="name">{{$combineditem_s->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank a">a</td>
                    <td>
                        @foreach ($combineditems_a as $combineditem_a)
                        <div class="items-chip">
                        <a class="chip {{$combineditem_a->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_a->id}}">
                                <img src="/img/items/combined/{{$combineditem_a->img}}">
                                <span class="name">{{$combineditem_a->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank b">b</td>
                    <td>
                        @foreach ($combineditems_b as $combineditem_b)
                        <div class="items-chip">
                        <a class="chip {{$combineditem_b->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_b->id}}">
                                <img src="/img/items/combined/{{$combineditem_b->img}}">
                                <span class="name">{{$combineditem_b->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank c">c</td>
                    <td>
                        @foreach ($combineditems_c as $combineditem_c)
                        <div class="items-chip">
                        <a class="chip {{$combineditem_c->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_c->id}}">
                                <img src="/img/items/combined/{{$combineditem_c->img}}">
                                <span class="name">{{$combineditem_c->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank d">d</td>
                    <td>
                        @foreach ($combineditems_d as $combineditem_d)
                        <div class="items-chip">
                        <a class="chip {{$combineditem_d->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_d->id}}">
                                <img src="/img/items/combined/{{$combineditem_d->img}}">
                                <span class="name">{{$combineditem_d->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank e">e</td>
                    <td>
                        @foreach ($combineditems_e as $combineditem_e)
                        <div class="items-chip">
                        <a class="chip {{$combineditem_e->updown}} modal-trigger combined" href="#modal_combined_item" data-id="{{$combineditem_e->id}}">
                                <img src="/img/items/combined/{{$combineditem_e->img}}">
                                <span class="name">{{$combineditem_e->name}}</span>
                                <span class="hide"></span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- Modal Structure -->
    <div id="modal_list_combined" class="modal modal-list-combined bottom-sheet">
        <div class="container">
            <div class="modal-content">
                <div class="col s12 center-align hide-on-med-and-up mobile-close-modal">
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons right">close</i>Đóng lại</a>
                </div>           
                <div class="col s12 m6 margin-bottom">
                    <img id="item_img" />
                    <h4 id="item_name"></h4>
                </div>
                <div class="col s12 m6 right-align hide-on-med-and-down">
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons right">close</i>Đóng lại</a>
                </div>
                <table class="striped">
                    <thead>
                        <tr>
                            <th class="center-align">Kết hợp</th>
                            <th class="left-align">Kết hợp lại thành</th>
                            <th class="center-align">Xếp hạng</th>
                        </tr>
                    </thead>

                    <tbody>
                            @foreach ($baseitems as $baseitem)
                            <tr class="row-{{$baseitem->id}}">
                                <td class="center-align">
                                    <a class="item">
                                        <img src="/img/items/base/{{$baseitem->img}}">
                                    </a>
                                </td>
                                <td class="left-align">
                                    <a class="item combined">
                                        <img>
                                    </a>
                                    <a class="info"></a>
                                </td>
                                <td class="center-align modal-column-rank"></td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
            <!-- <div class="modal-footer">
                
            </div> -->
        </div>
    </div>
    <div id="modal_combined_item" class="modal modal-list-combined bottom-sheet">
        <div class="container">
            <div class="modal-content">
                <div class="col s12 center-align hide-on-med-and-up mobile-close-modal">
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons right">close</i>Đóng lại</a>
                </div>           
                <div class="col s12 l6 margin-bottom">
                    <img id="item_img_combined" />
                    <h4 id="item_name_combined"></h4>
                </div>
                <div class="col s12 l6 right-align hide-on-med-and-down">
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons right">close</i>Đóng lại</a>
                </div>
                <table class="striped">
                    <thead>
                        <tr>
                            <th class="center-align">Kết hợp</th>
                            <th class="left-align">Kết hợp lại thành</th>
                            <th class="center-align">Xếp hạng</th>
                        </tr>
                    </thead>

                    <tbody>
                            <tr>
                                <td class="center-align">
                                    <a class="item">
                                        <img id="base1" src="/img/items/base/bf.png">
                                    </a>
                                    <a class="item">
                                        <img id="base2" src="/img/items/base/bf.png">
                                    </a>
                                </td>
                                <td class="left-align">
                                    <a class="item combined hide-on-small-only">
                                        <img>
                                    </a>
                                    <a class="info"></a>
                                </td>
                                <td class="center-align modal-column-rank"></td>
                            </tr>
                    </tbody>
                </table>
            </div>
            <!-- <div class="modal-footer">
                
            </div> -->
        </div>
    </div>
</div>
<script>
    function change_alias(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    str = str.trim(); 
    return str;
    }
    $(document).ready(function() {
        $('#liItems').addClass("active");
        window.baseitems = {!!$baseitems!!};
        window.combineditems = {!!$combineditems!!};
        $('.items-chip .chip .name').each(function (index) {
            var str = $(this).text();
            var str_low = change_alias(str);
            $(this).next().text(str_low);
      });
    });
</script>
@endsection