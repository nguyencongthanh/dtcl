@extends('layouts.front')

@section('content')
<script src="{{ asset('js/champions.js') }}" defer></script>
<title>Tướng - Đấu trường chân lý</title>
<div class="row">

    <div id="filter" class="col s12 l3">
        <div class="col s12 line-bottom top-col">
            <div class="col s6 left-align">
                <h2 class="title">Bộ lọc</h2>
            </div>
            <div class="col s6 right-align">
                <a id="filter_clear" class="btn-floating waves-effect waves-light blue"><i class="material-icons">close</i></a>
            </div>
        </div>
        <div class="col s12 no-padding">
            <ul class="collapsible popout filter">
                <li class="active">
                    <div class="collapsible-header"><i class="material-icons">bubble_chart</i><span>Tiền</span></div>
                    <div class="collapsible-body">
                        <form id="filter_cost">
                            <p>
                                <label>
                                    <input type="checkbox" value="1" />
                                    <span>$ 1 tiền</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input type="checkbox" value="2" />
                                    <span>$ 2 tiền</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input type="checkbox" value="3" />
                                    <span>$ 3 tiền</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input type="checkbox" value="4" />
                                    <span>$ 4 tiền</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input type="checkbox" value="5" />
                                    <span>$ 5 tiền</span>
                                </label>
                            </p>
                        </form>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">spa</i><span>Tộc</span></div>
                    <div class="collapsible-body">
                        <form id="filter_origin">
                            @foreach ($origins as $origin)
                            <p>
                                <label>
                                    <input type="checkbox" value="{{$origin->id}}" />
                                    <span class="text-cap"><img src="/img/origins/{{$origin->abilities_img}}">{{$origin->name}}</span>
                                </label>
                            </p>
                            @endforeach
                        </form>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">brightness_2</i><span>Hệ</span></div>
                    <div class="collapsible-body">
                        <form id="filter_class">
                            @foreach ($classes as $class)
                            <p>
                                <label>
                                    <input type="checkbox" value="{{$class->id}}" />
                                    <span class="text-cap"><img src="/img/classes/{{$class->abilities_img}}">{{$class->name}}</span>
                                </label>
                            </p>
                            @endforeach
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="col s12 l9 line-left">
        <div class="col s12 line-bottom top-col">
            <div class="col s12 m6 left-align">
                <h2 class="title">Danh sách các tướng</h2>
            </div>
            <div class="col m6 right-align hide-on-med-and-down">
                <div class="input-field search-champions">
                    <input id="search" type="text" class="validate autocomplete">
                    <label for="search">Tìm kiếm</label>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="col s6 left-align">
                <h3 class="title">Phiên bản hiện tại 9.18</h3>
            </div>
            <div class="col s6 right-align">
                <span class="text-up-rank">Tăng sức mạnh</span> <span class="text-down-rank">Giảm sức mạnh</span>
            </div>
        </div>
        <table id="table_champions">
            <tbody>
                <tr class="row-rank">
                    <td class="column-rank s">s</td>
                    <td>
                        @foreach ($champions_s as $champion_s)
                        <div class="champions-chip" data-id="{{$champion_s->id}}" data-cost="{{$champion_s->cost}}" data-origin="{{$champion_s->origin}}" data-class="{{$champion_s->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_s->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_s->avatar}}">
                                <span class="name">{{$champion_s->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank a">a</td>
                    <td>
                        @foreach ($champions_a as $champion_a)
                        <div class="champions-chip" data-id="{{$champion_a->id}}" data-cost="{{$champion_a->cost}}" data-origin="{{$champion_a->origin}}" data-class="{{$champion_a->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_a->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_a->avatar}}">
                                <span class="name">{{$champion_a->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank b">b</td>
                    <td>
                        @foreach ($champions_b as $champion_b)
                        <div class="champions-chip" data-id="{{$champion_b->id}}" data-cost="{{$champion_b->cost}}" data-origin="{{$champion_b->origin}}" data-class="{{$champion_b->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_b->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_b->avatar}}">
                                <span class="name">{{$champion_b->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank c">c</td>
                    <td>
                        @foreach ($champions_c as $champion_c)
                        <div class="champions-chip" data-id="{{$champion_c->id}}" data-cost="{{$champion_c->cost}}" data-origin="{{$champion_c->origin}}" data-class="{{$champion_c->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_c->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_c->avatar}}">
                                <span class="name">{{$champion_c->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank d">d</td>
                    <td>
                        @foreach ($champions_d as $champion_d)
                        <div class="champions-chip" data-id="{{$champion_d->id}}" data-cost="{{$champion_d->cost}}" data-origin="{{$champion_d->origin}}" data-class="{{$champion_d->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_d->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_d->avatar}}">
                                <span class="name">{{$champion_d->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
                <tr class="row-rank">
                    <td class="column-rank e">e</td>
                    <td>
                        @foreach ($champions_e as $champion_e)
                        <div class="champions-chip" data-id="{{$champion_e->id}}" data-cost="{{$champion_e->cost}}" data-origin="{{$champion_e->origin}}" data-class="{{$champion_e->class}}" show-hide="1">
                            <a class="chip sidenav-trigger {{$champion_e->updown}}" data-target="profile">
                                <img src="/img/champions/{{$champion_e->avatar}}">
                                <span class="name">{{$champion_e->name}}</span>
                            </a>
                        </div>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <ul id="profile" class="sidenav">
        <div id="profile_rank" class="rank">
            <a class="btn-floating btn-large pulse"></a>
        </div>
        <div class="col s12 right-align close-mobile">
            <a class="waves-effect waves-light btn red sidenav-close"><i class="material-icons left">close</i>Đóng lại</a>
        </div>
        <li class="col s12 center-align">
            <div class="col s12 l6 center-align">
                <img id="profile_avatar" class="z-depth-4 avatar">
                <h2 id="profile_name"></h2>
            </div>
            <div class="col s12 l6 center-align margin-top">
                <div class="col s12 margin-bottom">
                    <div class="col s6 left-align">
                        <img width="24px" height="24px" src="{{ url('/img/others/health.png') }}" />
                        <span class="mana-text">Máu</span>
                    </div>
                    <div class="col s6 right-align">
                        <b><span id="profile_health"></span></b>
                    </div>
                    <div class="progress health">
                        <div class="determinate" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col s12 margin-bottom">
                    <div class="col s6 left-align">
                        <img width="24px" height="24px" src="{{ url('/img/others/mana.png') }}" />
                        <span class="mana-text">Năng lượng</span>
                    </div>
                    <div class="col s6 right-align">
                        <b><span id="profile_sm"></span> / <span id="profile_mana"></span></b>
                    </div>
                    <div class="progress mana">
                        <div id="profile_mana_progress" class="determinate"></div>
                    </div>
                </div>
                <div class="col s12 margin-bottom">
                    <div class="col s6 left-align">
                        <img width="24px" height="24px" src="{{ url('/img/others/coin.png') }}" />
                        <span class="mana-text">Tiền</span>
                    </div>
                    <div class="col s6 right-align">
                        <b><span id="profile_cost"></span> / <span>5</span></b>
                    </div>
                    <div class="progress cost">
                        <div id="profile_cost_progress" class="determinate"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="col s12 center-align">
            <div class="col s12 l6 stats left-align">
                <div class="col m2 hide-on-small-only"></div>
                <div class="col s12 m8">
                    <ul class="collection">
                        <li class="collection-item">Giáp Cơ Bản: <span id="profile_armor"></span></li>
                        <li class="collection-item">Kháng Phép: <span id="profile_mr"></span></li>
                        <li class="collection-item">Sát thương vật lý: <span id="profile_damage"></span></li>
                        <li class="collection-item">Sát thương thời gian: <span id="profile_dps"></span></li>
                        <li class="collection-item">Tốc độ đánh: <span id="profile_as"></span></li>
                        <li class="collection-item">Tỉ lệ chí mạng: <span id="profile_cr"></span></li>
                        <li class="collection-item">Tầm đánh xa: <span id="profile_range"></span> ô</li>
                    </ul>
                </div>
                <div class="col m2 hide-on-small-only"></div>                
            </div>
            <div class="col s12 l6">
                <div class="card-panel grey lighten-5 z-depth-1">
                    <div class="row valign-wrapper">
                        <div class="col s2 champion-item-column">                            
                            <img data-position="left" class="responsive-img">
                            <img data-position="left" class="responsive-img">
                            <img data-position="left" class="responsive-img">
                        </div>
                        <div class="col s10 right-align">
                            <h5 id="profile_skill_name"></h5>
                            <img id="profile_skill_img" class="circle responsive-img">
                            <span id="profile_skill_info" class="black-text"></span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="col s12 champions-origin-class">
            <div class=" col s12 m12 l6 origin">
                <h4></h4>
                <div class="carousel"></div>
            </div>
            <div class=" col s12 m12 l6 class">
                <h4></h4>
                <div class="carousel"></div>
            </div>
        </li>
    </ul>

</div>
<script>
    $(document).ready(function() {
        $('#liChampions').addClass("active");
        window.champions = {!!$champions!!};
        window.origins = {!!$origins!!};
        window.classes = {!!$classes!!};
        window.champions_items = {!!$combineditems!!};
    });
</script>
@endsection