@extends('layouts.front')

@section('content')
<script src="{{ asset('js/teams.js') }}" defer></script>
<title>Trang bị - Đấu trường chân lý</title>
<div class="col s12 container-list-teams">
    <h4 class="version">Phiên bản hiện tại 9.18</h4>
    <ul id="teams_list" class="collapsible popout">
        @foreach ($teams_s as $team_s)
        <li class="row">
            <div class="collapsible-header col s12">
                <div class="col s12 col-champions">
                    <a class="btn-floating btn-large waves-effect waves-light badge-rank s">s</a>
                    <a class="c" data-c="{{$team_s->c1}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c2}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c3}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c4}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c5}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c6}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c7}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_s->c8}}">
                        <img class="avatar z-depth-4" />
                    </a>
                </div>
            </div>
            <div class="collapsible-body">
                <h2>{{$team_s->name}}</h2>
                <div class="row">
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_s->mvp1}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_s->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_s->mvp2}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_s->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_s->mvp3}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_s->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_s->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
        @foreach ($teams_a as $team_a)
        <li class="row">
            <div class="collapsible-header col s12">
                <div class="col s12 col-champions">
                    <a class="btn-floating btn-large waves-effect waves-light badge-rank a">a</a>
                    <a class="c" data-c="{{$team_a->c1}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c2}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c3}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c4}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c5}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c6}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c7}}">
                        <img class="avatar z-depth-4" />
                    </a>
                    <a class="c" data-c="{{$team_a->c8}}">
                        <img class="avatar z-depth-4" />
                    </a>
                </div>
            </div>
            <div class="collapsible-body">
                <h2>{{$team_a->name}}</h2>
                <div class="row">
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_a->mvp1}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_a->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp1}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_a->mvp2}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_a->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp2}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col s4 l4">
                        <table class="centered striped">
                            <tr>
                                <td>
                                    <a class="c" data-mvp="{{$team_a->mvp3}}">
                                        <img class="avatar z-depth-4" />
                                        <h5></h5>
                                    </a>
                                </td>
                            </tr>
                            <tr class="i">
                                <td>
                                    <a class="i" data-mvp="{{$team_a->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                    <a class="i" data-mvp="{{$team_a->mvp3}}">
                                        <img class="z-depth-1" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>
<script>
    $(document).ready(function() {
        $('#liTeams').addClass("active");
        window.items = {!!$items!!};
        window.champions = {!!$champions!!};
        window.teams = {!!$teams!!};
    });
</script>
@endsection