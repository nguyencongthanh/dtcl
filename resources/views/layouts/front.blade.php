<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Jquery Slim -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <!-- <title>{{ config('app.name', 'Laravel') }}</title> -->

    <!-- Scripts -->
    <script src="{{ asset('js/front.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">
</head>

<body>
    <nav>
        <div class="container">
            <div class="nav-wrapper">
                <a href="/" class="brand-logo">
                    <img src="/img/logo/1.png">
                </a>
                <ul id="nav-mobile" class="right hide-on-small-only">
                    <li id="liChampions"><a href="{{ url('/') }}"><i class="material-icons left">person_pin</i>Tướng</a></li>
                    <li id="liItems"><a href="{{ url('/items') }}"><i class="material-icons left">security</i>Trang bị</a></li>
                    <li id="liTeams"><a href="{{ url('/teams') }}"><i class="material-icons left">grain</i>Đội hình</a></li>
                </ul>
                <div class="right hide-on-med-and-up">
                    <a class='dropdown-trigger mnav waves-effect waves-light btn' href='#' data-target='mnav'>Tùy chọn</a>
                    <!-- Dropdown Structure -->
                    <ul id='mnav' class='dropdown-content'>
                        <li><a href="/champions"><i class="material-icons left">person_pin</i>Tướng</a></li>
                        <li><a href="/items"><i class="material-icons left">security</i>Trang bị</a></li>
                        <li><a href="/teams"><i class="material-icons left">grain</i>Đội hình</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <!-- Page Layout here -->
        @yield('content')
    </div>
</body>

</html>