$(document).ready(function () {
    //Search
    $("#search").keyup(function () {
        var txt = $(this).val().toLowerCase();
        $("#table_items tr td .items-chip").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(txt) > -1)
        });
    });
    //Base Items
    $('ul.items li a.item.base').click(function () { // click open base items
        var id = parseInt($(this).attr('data-id') - 1);
        $('#item_img').attr('src', '/img/items/base/' + baseitems[id].img);
        $('#item_name').text(baseitems[id].name);
        baseitems.forEach(element => {
            var found_item = $.grep(combineditems, function (v) {
                return (((v.item1 == (id + 1) && v.item2 == element.id) || (v.item2 == (id + 1) && v.item1 == element.id)));
            });
            $('#modal_list_combined table tbody tr.row-' + element.id + ' td a.combined img').attr('src', '/img/items/combined/' + found_item[0].img);
            $('#modal_list_combined table tbody tr.row-' + element.id + ' td a.info').text(found_item[0].abilities);
            $('#modal_list_combined table tbody tr.row-' + element.id + ' td.modal-column-rank').attr('data-rank', found_item[0].rank);
            $('#modal_list_combined table tbody tr.row-' + element.id + ' td.modal-column-rank').text(found_item[0].rank);
        });
    });
    //Combined Items
    $('a.chip.combined').click(function () { // click open combined items
        var id = parseInt($(this).attr('data-id')); //combined item id
        var found_combined_item = $.grep(combineditems, function (v) {
            return (v.id == id);
        });
        var found_base_item1 = $.grep(baseitems, function (v) {
            return (v.id == found_combined_item[0].item1);
        });
        var found_base_item2 = $.grep(baseitems, function (v) {
            return (v.id == found_combined_item[0].item2);
        });
        $('#base1').attr('src', '/img/items/base/' + found_base_item1[0].img);
        $('#base2').attr('src', '/img/items/base/' + found_base_item2[0].img);
        $('#modal_combined_item').attr('data-updown', found_combined_item[0].updown);
        $('#item_img_combined').attr('src','/img/items/combined/' + found_combined_item[0].img);
        $('#item_name_combined').text(found_combined_item[0].name);
        $('#modal_combined_item table tbody tr td a.item.combined img').attr('src', '/img/items/combined/' + found_combined_item[0].img);
        $('#modal_combined_item table tbody tr td a.info').text(found_combined_item[0].abilities);
        $('#modal_combined_item table tbody tr td.modal-column-rank').attr('data-rank', found_combined_item[0].rank);
        $('#modal_combined_item table tbody tr td.modal-column-rank').text(found_combined_item[0].rank);
    });
    $('a.red.item.modal-trigger').click(function () { // click open combined items
        var id = parseInt($(this).attr('data-id')); //combined item id
        var found_combined_item = $.grep(combineditems, function (v) {
            return (v.id == id);
        });
        var found_base_item1 = $.grep(baseitems, function (v) {
            return (v.id == found_combined_item[0].item1);
        });
        var found_base_item2 = $.grep(baseitems, function (v) {
            return (v.id == found_combined_item[0].item2);
        });
        $('#base1').attr('src', '/img/items/base/' + found_base_item1[0].img);
        $('#base2').attr('src', '/img/items/base/' + found_base_item2[0].img);
        $('#modal_combined_item').attr('data-updown', found_combined_item[0].updown);
        $('#item_img_combined').attr('src','/img/items/combined/' + found_combined_item[0].img);
        $('#item_name_combined').text(found_combined_item[0].name);
        $('#modal_combined_item table tbody tr td a.item.combined img').attr('src', '/img/items/combined/' + found_combined_item[0].img);
        $('#modal_combined_item table tbody tr td a.info').text(found_combined_item[0].abilities);
        $('#modal_combined_item table tbody tr td.modal-column-rank').attr('data-rank', found_combined_item[0].rank);
        $('#modal_combined_item table tbody tr td.modal-column-rank').text(found_combined_item[0].rank);
    });
});