//require('./bootstrap');
$(document).ready(function () {
    $('ul#teams_list li:first-child').addClass('active');
    $('.collapsible').collapsible();
    $('ul#teams_list li').each(function (index) {// find li
        $(this).find('.col-champions a.c').each(function (index) {// find col champions
            var id = $(this).attr('data-c');
            if(id){
                var found_c = $.grep(champions, function (v) {
                    return (v.id == parseInt(id));
                });
                $(this).children().attr('src', '/img/champions/' + found_c[0].avatar);
            }
            else{
                $(this).remove();
            }
        });//done
        $(this).find('.collapsible-body a.c').each(function (index) {// find col mvp champions
            var id = $(this).attr('data-mvp');
            if(id){
                var found_mvp = $.grep(champions, function (v) {
                    return (v.id == parseInt(id));
                });
                $(this).children('img').attr('src', '/img/champions/' + found_mvp[0].avatar);
                $(this).children('h5').text(found_mvp[0].name);
            }
            else{
                $(this).remove();
            }
        });//done
        $(this).find('.collapsible-body tr.i').each(function (index) {// find col mvp champions items            
            var id_mvp = $(this).find('a.i:first-child').attr('data-mvp'); //sure 1 champion have 3 items
            var found_mvp = $.grep(champions, function (v) {
                return (v.id == parseInt(id_mvp));
            });
            var found_item1 = $.grep(items, function (v) { // item 1
                return (v.id == parseInt(found_mvp[0].item1));
            });
            var found_item2 = $.grep(items, function (v) { // item 2
                return (v.id == parseInt(found_mvp[0].item2));
            });
            var found_item3 = $.grep(items, function (v) { // item 3
                return (v.id == parseInt(found_mvp[0].item3));
            });
            $(this).find('a.i:first-child img').attr('src', '/img/items/combined/' + found_item1[0].img);
            $(this).find('a.i:nth-child(2) img').attr('src', '/img/items/combined/' + found_item2[0].img);
            $(this).find('a.i:last-child img').attr('src', '/img/items/combined/' + found_item3[0].img);
        });//done
    });
});