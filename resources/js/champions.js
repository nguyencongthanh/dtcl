$(document).ready(function () {
    String.prototype.capitalize = function() {
        return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
    };
    //Champions
    $("#filter_clear").click(function () { // filter clear
        $('#filter').find('input:checkbox:checked').each(function (index) {
            this.click();
            $chips.each(function (index) {
                $(this).attr('show-hide', 1);
            });
        });
    });
    $('#filter_cost :checkbox').change(function () { // checkbox cost action
        var check_value = this.value;
        $chips = $('#table_champions').find('.champions-chip');
        if (this.checked) {
            // checked
            if ($('#filter').find('input:checkbox:checked').length <= 1) { // if first check
                $chips.each(function (index) {
                    if ($(this).attr('data-cost') == check_value) {
                        $(this).attr('show-hide', 1); // show checked data cost
                    }
                    else {
                        $(this).attr('show-hide', 0); // hide checked data cost
                    }
                });
            }
            else { // if multiple check
                $chips.each(function (index) {
                    if ($(this).attr('data-cost') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) + 1);
                    }
                });
            }
        }
        else {
            // unchecked
            if ($('#filter').find('input:checkbox:checked').length == 0) { // if clear check
                $chips.each(function (index) {
                    $(this).attr('show-hide', 1);
                });
            }
            else { // if uncheck 1 checkbox
                $chips.each(function (index) {
                    if ($(this).attr('data-cost') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) - 1);
                    }
                });
            }
        }
    });
    $('#filter_origin :checkbox').change(function () { // checkbox origin action
        var check_value = this.value;
        $chips = $('#table_champions').find('.champions-chip');
        if (this.checked) {
            // checked
            if ($('#filter').find('input:checkbox:checked').length <= 1) { // if first check
                $chips.each(function (index) {
                    if ($(this).attr('data-origin') == check_value) {
                        $(this).attr('show-hide', 1); // show checked data origin
                    }
                    else {
                        $(this).attr('show-hide', 0); // hide checked data origin
                    }
                });
            }
            else { // if multiple check
                $chips.each(function (index) {
                    if ($(this).attr('data-origin') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) + 1);
                    }
                });
            }
        }
        else {
            // unchecked
            if ($('#filter').find('input:checkbox:checked').length == 0) { // if clear check
                $chips.each(function (index) {
                    $(this).attr('show-hide', 1);
                });
            }
            else { // if uncheck 1 checkbox
                $chips.each(function (index) {
                    if ($(this).attr('data-origin') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) - 1);
                    }
                });
            }
        }
    });
    $('#filter_class :checkbox').change(function () { // checkbox class action
        var check_value = this.value;
        $chips = $('#table_champions').find('.champions-chip');
        if (this.checked) {
            // checked
            if ($('#filter').find('input:checkbox:checked').length <= 1) { // if first check
                $chips.each(function (index) {
                    if ($(this).attr('data-class') == check_value) {
                        $(this).attr('show-hide', 1); // show checked data class
                    }
                    else {
                        $(this).attr('show-hide', 0); // hide checked data class
                    }
                });
            }
            else { // if multiple check
                $chips.each(function (index) {
                    if ($(this).attr('data-class') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) + 1);
                    }
                });
            }
        }
        else {
            // unchecked
            if ($('#filter').find('input:checkbox:checked').length == 0) { // if clear check
                $chips.each(function (index) {
                    $(this).attr('show-hide', 1);
                });
            }
            else { // if uncheck 1 checkbox
                $chips.each(function (index) {
                    if ($(this).attr('data-class') == check_value) {
                        $(this).attr('show-hide', parseInt($(this).attr('show-hide')) - 1);
                    }
                });
            }
        }
    });
    //Profile
    $('a.chip.sidenav-trigger').click(function () { // click open profile
        var id = parseInt($(this).parent().attr('data-id')) - 1;
        var id_origin = origins[parseInt(champions[id].origin) - 1].id;
        var id_class = classes[parseInt(champions[id].class) - 1].id;
        var name_origin = origins[parseInt(champions[id].origin) - 1].name;
        var name_class = classes[parseInt(champions[id].class) - 1].name;
        var found_champion_item1 = $.grep(champions_items, function (v) {
            return (v.id == champions[id].item1);
        });
        var found_champion_item2 = $.grep(champions_items, function (v) {
            return (v.id == champions[id].item2);
        });
        var found_champion_item3 = $.grep(champions_items, function (v) {
            return (v.id == champions[id].item3);
        });
        $("#profile_rank").attr('data-rank', champions[id].rank);
        $("#profile_rank a").text(champions[id].rank);
        $("#profile_avatar").attr('src', '/img/champions/' + champions[id].avatar);
        $("#profile_name").text(champions[id].name);
        $("#profile_health").text(champions[id].health);
        $("#profile_sm").text(champions[id].sm);
        $("#profile_mana").text(champions[id].mana);
        $("#profile_cost").text(champions[id].cost);
        $("#profile_mana_progress").attr('style', 'width: ' + Math.round((champions[id].sm / champions[id].mana) * 100) + '%');
        $("#profile_cost_progress").attr('style', 'width: ' + Math.round((champions[id].cost / 5) * 100) + '%');
        $("#profile_armor").text(champions[id].armor);
        $("#profile_mr").text(champions[id].mr);
        $("#profile_damage").text(champions[id].damage);
        $("#profile_dps").text(champions[id].dps);
        $("#profile_as").text(champions[id].as);
        $("#profile_cr").text(champions[id].cr);
        $("#profile_range").text(champions[id].range);
        $(".champion-item-column img:first-child").attr('src', '/img/items/combined/' + found_champion_item1[0].img);
        var name_item1 = (found_champion_item1[0].name).capitalize();
        var name_item2 = (found_champion_item2[0].name).capitalize();
        var name_item3 = (found_champion_item3[0].name).capitalize();
        $(".champion-item-column img:first-child").attr('data-tooltip', name_item1);
        $(".champion-item-column img:nth-child(2)").attr('src', '/img/items/combined/' + found_champion_item2[0].img);
        $(".champion-item-column img:nth-child(2)").attr('data-tooltip', name_item2);
        $(".champion-item-column img:last-child").attr('src', '/img/items/combined/' + found_champion_item3[0].img);
        $(".champion-item-column img:last-child").attr('data-tooltip', name_item3);
        $("#profile_skill_img").attr('src', '/img/skills/' + champions[id].skill_img);
        $("#profile_skill_name").text(champions[id].skill_name);
        $("#profile_skill_info").html(champions[id].skill_info);
        $('li.champions-origin-class .origin .carousel a').remove();
        $('li.champions-origin-class .class .carousel a').remove();
        $('li.champions-origin-class .origin h4').text('Cùng Tộc ' + name_origin);
        $('li.champions-origin-class .class h4').text('Cùng Hệ ' + name_class);
        champions.forEach(element => {
            if ((element.origin == id_origin) && (element.id != (id + 1))) {
                $('li.champions-origin-class .origin .carousel').append('<a class="carousel-item"><img src="/img/champions/' + element.avatar + '"></a>');
                $('li.champions-origin-class .origin .carousel').carousel();
            }
        });
        champions.forEach(element => {
            if ((element.class == id_class) && (element.id != (id + 1))) {
                $('li.champions-origin-class .class .carousel').append('<a class="carousel-item"><img src="/img/champions/' + element.avatar + '"></a>');
                $('li.champions-origin-class .class .carousel').carousel();
            }
        });
        $('#profile').attr('data-updown', champions[id].updown);
        $('.champion-item-column img').tooltip();
    });
    //Search
    $("#search").keyup(function () {
        var txt = $(this).val().toLowerCase();
        $("#table_champions tr td .champions-chip").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(txt) > -1)
        });
    });
});
